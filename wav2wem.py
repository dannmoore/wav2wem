#
# File: wav2wem.py
# Description: Converts a .wav file to a .wem file for use with AudioKinetic's Wwise library
# Notes: Only supports RIFF .wav files in 44khz or 22khz PCM format.
#
# Copyright (C) 2019 Dann Moore.  All rights reserved.
#

import tkinter as tk
from tkinter import messagebox
from tkinter import filedialog
import sys


# define callback for our select input file button
def input_buttonCallBack():
   global infilename
   global outfilename
   
   infilename = filedialog.askopenfilename(initialdir = "./",title = "Select input file",filetypes = (("Wave files","*.wav"),("All files","*.*")))
   infilename_label["text"] = "Input Filename: " + infilename
   # set a default output file name
   outfilename = ".wem".join(infilename.rsplit(".wav", 1))
   outfilename_label["text"] = "Output Filename: " + outfilename


# define callback for our select output file button
def output_buttonCallBack():
   global filename
   outfilename = filedialog.asksaveasfilename(initialdir = "./", initialfile = outfilename, title = "Select output file",filetypes = (("WEM files","*.wem"),("All files","*.*")))   
   outfilename_label["text"] = "Output Filename: " + outfilename


# define callback for our convert button
def convert_buttonCallBack():
   global infilename
   global outfilename
   doconversion(infilename,outfilename)
   messagebox.showinfo("Completed", "Conversion complete!")



# define the conversion function
def doconversion(filename_in, filename_out):
   fout = open(filename_out, "wb")
   with open(filename_in, "rb") as fin:
      # Read four bytes are the file header
      byte4 = fin.read(4)
      if byte4 == b'RIFF': 
         print("Reading RIFF format...")
      else:
         print("Error: Non-RIFF formats not supported!")
         fin.close()
         fout.close()
         return

      # Read four bytes for the chunk size
      byte4 = fin.read(4)
      chunksize = int.from_bytes(byte4, byteorder='little')
      print("Chunk size = " + str(chunksize))

      # Read four bytes for "WAVE"
      byte4 = fin.read(4)
      if byte4 != b'WAVE':
         print("Error: Non-WAVE formats not supported!")
         fin.close()
         fout.close()
         return

      # Read four bytes for "fmt "
      byte4 = fin.read(4)
      if byte4 != b'fmt ':
         print("Error: Subchunk not detected!")
         fin.close()
         fout.close()
         return

      # Read four bytes for the format subchunk size
      byte4 = fin.read(4)
      fmtchunksize = int.from_bytes(byte4, byteorder='little')
      print("Fmt chunk size = " + str(fmtchunksize))

      # Read two bytes for the audio format
      byte2 = fin.read(2)
      audioformat = int.from_bytes(byte2, byteorder='little')
      print("Audio format = " + str(audioformat))
      if(audioformat != 1):  # PCM = 1, other formats not supported
         print("Error: Audio format not PCM!")
         fin.close()
         fout.close()
         return  

      # Read two bytes for the number of channels
      byte2 = fin.read(2)
      channels = int.from_bytes(byte2, byteorder='little')
      print("Number of channels = " + str(channels))      

      # Read four bytes for the sample rate
      byte4 = fin.read(4)
      samplerate = int.from_bytes(byte4, byteorder='little')  
      print("Sample rate = " + str(samplerate)) 

      # Read four bytes for the byte rate
      byte4 = fin.read(4)
      byterate = int.from_bytes(byte4, byteorder='little')  
      print("Byte rate = " + str(byterate)) 

      # Read two bytes for the block alignment
      byte2 = fin.read(2)
      blockalignment = int.from_bytes(byte2, byteorder='little')  
      print("Block Alignment = " + str(blockalignment)) 

      # Read two bytes for the bits per sample
      byte2 = fin.read(2)
      bitspersample = int.from_bytes(byte2, byteorder='little')  
      print("Bits per Sample = " + str(bitspersample)) 
    
      # Read four bytes for "data"
      byte4 = fin.read(4)
      if byte4 != b'data':
         print("Error: Data subchunk not detected!")
         fin.close()
         fout.close()
         return

      # Read four bytes for the data chunk size
      byte4 = fin.read(4)
      datachunksize = int.from_bytes(byte4, byteorder='little')  
      print("Data Chunk Size = " + str(datachunksize)) 


      # Begin writing .wem file
      print("Writing WEM file...")
      # First four bytes contain "RIFF" or "RIFX".  In this case we are not supporting "RIFX",
      # so write "RIFF" regardless
      fout.write(b'RIFF') 

      # Write four bytes for the chunk size.  Add 20 bytes for additional wem header data.
      fout.write((chunksize + 20).to_bytes(4, byteorder='little'))

      # Write "WAVE"
      fout.write(b'WAVE') 

      # Write "fmt "
      fout.write(b'fmt ') 

      # Write four bytes for the format chunk size.  Add 8 bytes for additional wem header data.
      fout.write((fmtchunksize + 8).to_bytes(4, byteorder='little'))

      # Write two bytes for the audio format.  WEM files use 0xFEFF for PCM.
      fout.write(b'\xFE\xFF') 

      # Write two bytes for the number of channels
      fout.write((channels).to_bytes(2, byteorder='little'))

      # Write four bytes for the sample rate
      fout.write((samplerate).to_bytes(4, byteorder='little'))

      # Write four bytes for the byte rate
      fout.write((byterate).to_bytes(4, byteorder='little'))

      # Write two bytes for the block alignment
      fout.write((blockalignment).to_bytes(2, byteorder='little'))

      # Write two bytes for the bits per sample
      fout.write((bitspersample).to_bytes(2, byteorder='little'))

      # Next 20 bytes contain the additional WEM header data we added to the chunksize before
      # Write 4 bytes 0x06000000, function is unknown
      fout.write(b'\x06\x00\x00\x00')

      # Next byte controls the number of channels 0x01 or 0x02
      if(channels == 1):
         fout.write(b'\x01')
      else:
         fout.write(b'\x02')
      
      # The next three bytes indicate the sample rate, 0x310000 for 44100, 0x410000 for 22050, others not tested for
      if(samplerate == 44100):
         fout.write(b'\x31\x00\x00')
      else:
         fout.write(b'\x41\x00\x00')

      # Next four bytes are literally "JUNK"
      fout.write(b'JUNK') 

      # Next four bytes function is unknown
      fout.write(b'\x04\x00\x00\x00')

      # Next four bytes function is also uknown
      fout.write(b'\x00\x00\x00\x00')

      # Write "data"
      fout.write(b'data') 

      # Write four bytes for the data chunk size
      fout.write((datachunksize).to_bytes(4, byteorder='little'))

      # Write each data byte from the input file to the output file
      for x in range(datachunksize):
         byte1 = fin.read(1)
         fout.write(byte1)

   fin.close()
   fout.close()
   print("Conversion complete!")




# Initialize variables
infilename = "none"
outfilename = "none"


# Parse command-line arguments
if(len(sys.argv) == 2): # Error out if only one filename specified
   print("Error, both input and output filenames must be specified!")
   quit()
elif(len(sys.argv) >= 3): # Process using filenames, ignoring any further command line options
   infilename = sys.argv[1]
   outfilename = sys.argv[2]   
   doconversion(infilename,outfilename)
   quit()

# Else create the gui

# create tkinter controls
root = tk.Tk()
root.title("wav2wem")


infilename_label = tk.Label(root, text="Input Filename: " + infilename)
infilename_label.pack()

outfilename_label = tk.Label(root, text="Output Filename: " + outfilename)
outfilename_label.pack()

input_button = tk.Button(root, text ="Choose Input File", command = input_buttonCallBack)
input_button.pack()

output_button = tk.Button(root, text ="Choose Output File", command = output_buttonCallBack)
output_button.pack()

convert_button = tk.Button(root, text ="Convert!", command = convert_buttonCallBack)
convert_button.pack()


# begin tkinter gui loop
root.mainloop()

