# wav2wem

### Overview:

wav2wem is a small tool to convert standard .wav files to AudioKinetic's Wwise .wem format.  wav2wem is compatible with Python 3.0 or above.

### Usage:

For a simple GUI run "python.exe wav2wem.py"

Alternatively you may specify the input and output filenames on the command line by running "python.exe wav2wem.py <infile> <outfile>"


### Notes:

Wave files must be in either 44Khz or 22Khz and RIFF format.  RIFX is not supported.


### Legal:
Copyright (C) 2024 Dann Moore.  All rights reserved.




